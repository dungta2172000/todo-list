import Koa from 'koa';
import Router from 'koa-router';
import override from 'koa-methodoverride';
import parser from 'koa-bodyparser';
import mongoose from 'mongoose';
import todoRouter from './routers/todo';
import next from 'next';

const server = new Koa();
const rootRouter = new Router();

//config server next js
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handler = app.getRequestHandler();

app.prepare().then(() => {
  //prefix the root url is /
  
  rootRouter.use('/todo-list', todoRouter.routes());
  
  rootRouter.get('(.*)', async ctx => {
    await handler(ctx.req, ctx.res);
    ctx.respond = false;
});

  //this method use to prevent some method cause reloading page by changing the old to another but still detected the original one
  server.use(override('_method'));
  //read form from browser
  server.use(parser());
  server.use(rootRouter.routes());

  //ket noi server to database mongo
  const PORT = process.env.PORT || 4000;
  const MONGO_URI =
    'mongodb+srv://anhdung21:2107Asdfghjkl@main.0y66s.mongodb.net/todo-list?retryWrites=true&w=majority';

  const openServer = async () => {
    try {
      await mongoose.connect(MONGO_URI, {
        //tham so nay dung de tranh mot so warning tu mongo
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      server.listen(PORT, () => {
        console.log(`server listen on port : ${PORT}`);
      });
    } catch (e) {
      console.log('cant connect to server', e);
    }
  };
  //chan warning tu mongodb
  mongoose.set('useFindAndModify', false);

  openServer();
});
