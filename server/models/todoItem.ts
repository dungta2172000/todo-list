import mongoose from 'mongoose';

const Schema = mongoose.Schema;

//create schema an todo model

//create schema - giong nhu mot cai interface de dinh nghia data tren mongodb
const todoItemSchema = new Schema(
  {
    content: String,
    isDone: Boolean,
  },
  {
    timestamps: true,
  }
);

//create model - cai khung de san xuat ra cac instance dua tren schema va gan vao vi tri cu the tren mongodb
const TodoModel = mongoose.model('todos', todoItemSchema);

export default TodoModel;
