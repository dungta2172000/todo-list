import Router from 'koa-router';
import TodoModel from '../models/todoItem';

const todoRouter = new Router();

//tao CRUD data len mongodb

//get all todo list---------
todoRouter.get('/', async (ctx) => {
  try {
    const todoList = await TodoModel.find().lean();
    ctx.body = todoList;    
  } catch {
    console.log('fail to query from data');
  }
});
//done

// get one by id-----------
todoRouter.get('/:id', async (ctx) => {
  try {
    const todoItem = await TodoModel.findById(ctx.params.id).lean();
    ctx.body = todoItem
  } catch {
    console.log('fail to query from data');
  }
});
//done

//post data-------------
todoRouter.post('/add', async (ctx) => {
  try {
    const todoAdded = await TodoModel.create(ctx.request.body);
    ctx.body = todoAdded
  } catch (e) {
    console.log('fail to post data : ', e);
  }
});

todoRouter.post('/', async (ctx) => {
  try {
    const todoAdded = await TodoModel.create(ctx.request.body);
    ctx.body = todoAdded;
  } catch (e) {
    console.log('fail to post data : ', e);
  }
});
//done

//update todo------------
todoRouter.put('/:id', async (ctx) => {
  try {
    const {content, isDone} = ctx.request.body as any
    const todoUpdate = await TodoModel.findByIdAndUpdate(
      ctx.params.id,
      {content : content,
      isDone : isDone},
      { new: true }
    );
    ctx.body = todoUpdate
  } catch {
    console.log('fail to update todo');
  }
});
//done

//delete-------------------
todoRouter.delete('/:id', async (ctx) => {
  try {
    const id = ctx.params.id;
    const todoDelete = await TodoModel.findByIdAndRemove(id);
    ctx.body = todoDelete
  } catch {
    console.log('cant delete this item');
  }
});
//done

export default todoRouter;
