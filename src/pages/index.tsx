import React from 'react';
import TodoList from '../components/TodoList';
import TodoInput from '../components/TodoInput';

import '@shopify/polaris/dist/styles.css';
const HomePage: React.FunctionComponent = () => {
  return (
    <div>
      <TodoInput />
      <TodoList />
    </div>
  );
};

export default HomePage;
