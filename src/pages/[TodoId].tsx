import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  Button,
  Checkbox,
  Form,
  FormLayout,
  TextField,
  Card,
} from '@shopify/polaris';
import { MobileBackArrowMajor } from '@shopify/polaris-icons';
import PopupEdit from '../components/Popup';
import { observer } from 'mobx-react';
import { todoListModel } from '../models/todoList';
import { todoModel } from '../models/todoItem';

const TodoDetailPage = observer(() => {
  const router = useRouter();
  const isInit = React.useRef<boolean>(false);
  //check url trong url sau do thi
  React.useEffect(() => {
    if (router.isReady && !isInit.current) {
      const getData = async () => {
        try {
          const todoId = router.query.TodoId as string;
          todoModel.getTodo(todoId);
        } catch (e) {
          console.log('cant query', e);
        }
      };
      isInit.current = true;
      getData();
    }
  }, [router.isReady, router.query.TodoId]);

  const changeCheckbox = () => {
    todoModel.setIsDone();
  };

  const onChangeTextField = (e: string) => {
    todoModel.setText(e);
  };

  const handleChangeTodo = async () => {
    try {
      todoListModel.setLoading(true);
      await todoModel.updateTodo(todoModel);
      todoListModel.setLoading(false);
      router.replace('/');
    } catch {
      console.log('cant update todo');
    }
  };

  const handleBack = () => {
    router.push('/');
  };

  return router.query.TodoId === 'add' ? (
    <PopupEdit />
  ) : (
    <div className='Polaris-Page'>
      <div className='Polaris-Page__Content'>
        <Card sectioned={true}>
          <Form onSubmit={handleChangeTodo}>
            <FormLayout>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button icon={MobileBackArrowMajor} onClick={handleBack}>
                  Back
                </Button>
                <Button onClick={handleChangeTodo} primary>
                  Save
                </Button>
              </div>

              <TextField
                value={todoModel.content}
                onChange={onChangeTextField}
                label='Todo content'
              />

              <Checkbox
                label='Progress'
                checked={todoModel.isDone}
                onChange={changeCheckbox}
              />
            </FormLayout>
          </Form>
        </Card>
      </div>
    </div>
  );
});

export default TodoDetailPage;
