import React from 'react';
import { TextField, Form, Button, Card } from '@shopify/polaris';

import { useRouter } from 'next/router';
import { todoListModel } from '../../models/todoList';
import TodoModel from '../../models/todoItem';
import { observer } from 'mobx-react';

const addTodoModel = TodoModel.create();

const PopupEdit = observer(() => {
  const router = useRouter();

  //add to do task to db
  const handleAddTodo = async (e: React.FormEvent) => {
    e.preventDefault();
    if (addTodoModel.content == '') {
      alert(`can't operate empty Todo`);
      return;
    }
    todoListModel.setLoading(true);
    todoListModel.addTodo(addTodoModel.content);
    addTodoModel.setText('');
    todoListModel.setLoading(false);
    router.replace('/');
  };

  //onchange input process
  const onChangeTextField = (e: string) => {
    addTodoModel.setText(e);
  };

  //back button
  const handleBack = () => {
    router.push('/');
  };

  return (
    <div className='Polaris-Page'>
      <div className='Polaris-Page__Content'>
        <Card title='Add some todo' sectioned={true}>
          <Form onSubmit={handleAddTodo}>
            <TextField
              value={addTodoModel.content}
              onChange={(e) => onChangeTextField(e)}
              label='New todo'
              type='text'
            />
            <br />
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <Button onClick={handleBack}>Back</Button>
              {todoListModel.loading ? (
                <Button loading>Add</Button>
              ) : (
                <Button submit primary>
                  Add
                </Button>
              )}
            </div>
          </Form>
        </Card>
      </div>
    </div>
  );
});

export default PopupEdit;
