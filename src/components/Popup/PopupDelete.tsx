import React from 'react';
import { Modal } from '@shopify/polaris';
import { todoListModel } from '../../models/todoList';
import { TSSnapshotTodoModel } from 'src/models/todoItem';

interface PopupDeleteProps {
  handleChange: () => void;
  active: boolean;
  deleteTodo: (todoId: string) => void;
  todo: TSSnapshotTodoModel;
}
const PopupDelete: React.FC<PopupDeleteProps> = ({
  handleChange,
  active,
  todo,
}) => {
  const handleDeleteTodo = () => {
    console.log(todo);

    todoListModel.deleteTodo(todo.id);
    handleChange();
  };

  return (
    <div style={{ height: '500px', position: 'fixed' }}>
      <Modal
        open={active}
        onClose={handleChange}
        title='Do you want to delete this Todo'
        primaryAction={{
          content: 'Confirm',
          onAction: handleDeleteTodo,
        }}
      ></Modal>
    </div>
  );
};

export default PopupDelete;
