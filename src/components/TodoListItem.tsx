import React, { useState, useCallback } from 'react';
import Link from 'next/link';
import '@shopify/polaris/dist/styles.css';
import { Button, Icon } from '@shopify/polaris';
import { useRouter } from 'next/router';
import PopupDelete from './Popup/PopupDelete';
import { todoListModel } from '../models/todoList';
import { TickMinor, CancelSmallMinor } from '@shopify/polaris-icons';
import { TSSnapshotTodoModel } from '../models/todoItem';

//define structure of todo props
interface TodoListItemProps {
  todo: TSSnapshotTodoModel;
}

const TodoListItem: React.FunctionComponent<TodoListItemProps> = ({ todo }) => {
  const router = useRouter();
  // const [todoModel, setTodoModel] = useState(TodoModel.create(todo));

  const [active, setActive] = useState(false);

  //push tp another page to edit task todo
  const editTodo = () => {
    router.push(`/${todo.id}`);
  };

  const handelCloneTodo = (todo) => {
    todoListModel.setLoading(true);
    todoListModel.addTodo(todo.content);
    todoListModel.setLoading(false);
  };

  const deleteTodo = () => {
    console.log('delete');
  };
  //toggle popup open or close
  const handleDelete = useCallback(() => setActive(!active), [active]);

  return (
    <>
      <tr className='Polaris-DataTable__TableRow Polaris-DataTable--hoverable'>
        <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn'>
          {String(todo.id).substring(todo.id.length - 2)}
        </td>
        <th
          className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop '
          scope='row'
        >
          <Link href={`/${todo.id}`}>
            <a
              className='Polaris-Link Polaris-Link--removeUnderline'
              data-polaris-unstyled='true'
            >
              {todo.content}
            </a>
          </Link>
        </th>

        <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop '>
          {todo.isDone ? (
            <Icon source={TickMinor} color='success' />
          ) : (
            <Icon source={CancelSmallMinor} color='critical' />
          )}
        </td>
        <td
          className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop'
          style={{ display: 'flex', justifyContent: 'space-evenly' }}
        >
          <Button size='slim' onClick={editTodo}>
            Edit
          </Button>
          {todoListModel.loading ? (
            <Button size='slim' loading>
              Clone
            </Button>
          ) : (
            <Button size='slim' onClick={() => handelCloneTodo(todo)}>
              Clone
            </Button>
          )}

          <Button size='slim' destructive onClick={handleDelete}>
            Delete
          </Button>
        </td>
        <PopupDelete
          todo={todo}
          active={active}
          handleChange={handleDelete}
          deleteTodo={deleteTodo}
        />
      </tr>
    </>
  );
};

export default TodoListItem;
