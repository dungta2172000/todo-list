import React, { useEffect } from 'react';
import TodoListItem from './TodoListItem';
import { observer } from 'mobx-react';
import { todoListModel } from '../models/todoList';
import { getSnapshot } from 'mobx-state-tree';
import { TSSnapshotTodoModel } from '../models/todoItem';

const TodoList = observer(() => {
  useEffect(() => {
    todoListModel.getTodo();
  }, []);

  return (
    <>
      {todoListModel ? (
        <div>
          <div className='Polaris-Page'>
            <div className='Polaris-Page__Content'>
              <div className='Polaris-Card'>
                <div className='Polaris-DataTable--condensed'>
                  <div className='Polaris-DataTable Polaris-DataTable--condensed'>
                    <div className='Polaris-DataTable__ScrollContainer'>
                      <table className='Polaris-DataTable__Table'>
                        <thead>
                          <tr>
                            <th
                              data-polaris-header-cell='true'
                              className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--firstColumn'
                              scope='col'
                            >
                              Id
                            </th>
                            <th
                              data-polaris-header-cell='true'
                              className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header  '
                              scope='col'
                            >
                              Todo content
                            </th>

                            <th
                              data-polaris-header-cell='true'
                              className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header '
                              scope='col'
                            >
                              Check
                            </th>
                            <th
                              data-polaris-header-cell='true'
                              className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header '
                              scope='col'
                            >
                              Operation
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {getSnapshot(todoListModel.todos).map(
                            (todo: TSSnapshotTodoModel) => {
                              return <TodoListItem key={todo.id} todo={todo} />;
                            }
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
});

export default TodoList;
