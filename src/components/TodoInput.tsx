import React, { useState } from 'react';

import { Button } from '@shopify/polaris';

import { useRouter } from 'next/router';

const TodoInput = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  //function to open popup add todo
  const togglePopup = () => {
    setLoading(true);
    router.push('/add');
  };

  return (
    <div className='Polaris-Page'>
      <div className='Polaris-Page__Content'>
        <div>
          <div
            className='Polaris-Card'
            style={{ display: 'flex', justifyContent: 'space-between' }}
          >
            <div className='Polaris-Card__Header'>
              <h2 className='Polaris-Heading'>Add some new todo!</h2>
            </div>
            <div className='Polaris-Card__Section'>
              {loading ? (
                <Button loading>Add</Button>
              ) : (
                <Button onClick={() => togglePopup()} primary>
                  Add
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TodoInput;
