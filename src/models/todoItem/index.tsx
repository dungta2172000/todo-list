import { flow, Instance, types } from 'mobx-state-tree';
import api from '../../utils/api';

const TodoModel = types
  .model({
    id: types.optional(types.string, ''),
    content: types.optional(types.string, ''),
    isDone: types.optional(types.boolean, false),
  })
  .actions((self) => ({
    getTodo: flow(function* (todoId: string) {
      try {
        const promise = yield api.get(`/todo-list/${todoId}`);
        const data = promise.data;
        (self.id = String(data._id)),
          (self.content = data.content),
          (self.isDone = data.isDone);
      } catch (e) {
        console.log('fail to load data', e);
      }
    }),
    setText(text: string) {
      self.content = text;
    },
    setIsDone() {
      self.isDone = !self.isDone;
    },

    updateTodo: flow(function* (todo) {
      try {
        yield api.put(`/todo-list/${todo.id}`, {
          _id: todo.id,
          content: todo.content,
          isDone: todo.isDone,
        });
      } catch (e) {
        console.log('cant change', e);
      }
    }),
  }));

export default TodoModel;
export const todoModel = TodoModel.create();

export type TSSnapshotTodoModel = Instance<typeof TodoModel>;
