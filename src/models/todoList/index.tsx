import { types, flow, getSnapshot } from 'mobx-state-tree';
import TodoModel from '../todoItem';
import api from '../../utils/api';

const TodoListModel = types
  .model('TodoList', {
    todos: types.array(TodoModel),
  })
  // khoi tao bein tma thoi trong model , nhung khi goi getSnapshot thi se khong tao ra bien nay
  .volatile<{
    loading: boolean;
  }>(() => ({
    loading: false,
  }))

  .actions((self) => ({
    setLoading(status: boolean) {
      self.loading = status;
    },

    getTodo: flow(function* () {
      try {
        const dataLoaded = yield api.get('/todo-list');
        self.todos = dataLoaded.data.map((todo: TodoLoaded) => ({
          ...todo,
          id: String(todo._id),
        }));
      } catch (e) {
        console.log('fail to load data', e);
      }
    }),

    addTodo: flow(function* (todoContent: string) {
      try {
        const res = yield api.post('/todo-list', {
          content: todoContent,
          isDone: false,
        });
        const data = res.data;
        self.todos.push({
          id: String(data._id),
          content: data.content,
          isDone: data.isDone,
        });
      } catch (e) {
        console.log('cant push new todo', e);
      }
    }),

    deleteTodo: flow(function* (todoId: string) {
      try {
        const res = yield api.delete(`/todo-list/${todoId}`);
        console.log(res);
        const todoDeleted = self.todos.filter((todo) => {
          todo.id == todoId;
        });
        self.todos.splice(self.todos.indexOf(todoDeleted[0]), 1);
      } catch (e) {
        console.log('cant delete', e);
      }
    }),
  }))

  .views((self) => ({
    get allTodos() {
      return self.todos;
    },
  }));

export const todoListModel = TodoListModel.create();
