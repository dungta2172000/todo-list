import axios from 'axios';

const instance = axios.create();
instance.interceptors.request.use(async (config) => {
  config.baseURL = 'http://localhost:4000';
  return config;
});
export default instance;
