type TodoLoaded = {
  _id : string 
  content : string 
  isDone : boolean
}

type ToggleTodo = (todoSelected : TodoLoaded) => void 

type TogglePopup = () => void 